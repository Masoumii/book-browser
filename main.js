$(function(){

    /* Main App Settings - Adjust to your wishes */
    const animationSpeed = 750; // Animation speed in miliseconds
    const PDFFolder      = "pdf";  // Folder where PDF's can be located
    const startAtPage    = 1; // The Initial PDF page to show
    const zoomLevel      = 100; // Initial PDF-viewer zoom level percentage

    /* Global variables */
    var id = 1;
    var minId = 1;
    var maxId = 1;
    var containerWidth = $("body").width();

    /* Assign elements to variables */
    var bookImage       = $(".book-image");
    var bookTitle       = $(".book-title");
    var bookAuthor      = $(".book-author");
    var bookEdition     = $(".book-edition");
    var bookDescription = $("#bookSrc");

    /* Initial book load */
    pullBooks();

    /* Count all rows to determine maxId */
    $.get("countBooks.php", function(data){})

    .done(function(data){
        maxId = data;
        countBooks();
    })

    /* Get the next row */
    $(".next").on("click", function(){
        
        /* if current book is not the last book on the list, get the next book on the list */
        if(id != maxId){
            $(".next").hide();
            id += 1;
            countBooks();
            pullBooks();

            /* Animate */
            $("#book-container").css({"transform":"scale(0.4)","transition":"transform 0.5s"}).animate({"right":"+="+containerWidth+"px"}, {duration:animationSpeed,complete: function(){
                $(".next, .previous").fadeIn();
                $(this).animate({"right":"-"+containerWidth+"px"}, 1);
                $(this).animate({"right":"+="+containerWidth+"px"}, animationSpeed).css({"transform":"scale(1)","transition":"transform 0.5s"});
            }})
           
        }else{
            $(".next").hide();
            id = 1;
            countBooks();
            pullBooks();

            /* Animate */
            $("#book-container").css({"transform":"scale(0.4)","transition":"transform 0.5"}).animate({"right":"+="+containerWidth+"px"}, {duration:animationSpeed,complete: function(){
                $(".next, .previous").fadeIn();
                $(this).animate({"right":"-"+containerWidth+"px"}, 1);
                $(this).animate({"right":"+="+containerWidth+"px"}, animationSpeed).css({"transform":"scale(1)","transition":"transform 0.5"});
            }})
        }

    })

    /* Get the previous row */
    $(".previous").on("click", function(){

        /* if current book is not the first book on the list, get the previous book on the list */
        if(id !== minId){

            $(".previous").hide();
            id -= 1;
            countBooks();
            pullBooks();

            $("#book-container").css({"transform":"scale(0.4)","transition":"transform 0.5s"}).animate({"right":"-="+containerWidth+"px"}, {duration:animationSpeed,complete: function(){
                $(".next, .previous").fadeIn();
                $(this).animate({"right":"+"+containerWidth+"px"}, 1);
                $(this).animate({"right":"-="+containerWidth+"px"}, animationSpeed).css({"transform":"scale(1)","transition":"transform 0.5s"});
                $(this).css({
                    "transform":"scale(1)",
                    "transition":"transform 0.5"
                })

            }})
            
        }else if(id == maxId){
            $(".previous").hide();
            id = maxId;
            countBooks();
            pullBooks();

             /* Animate */
            $("#book-container").css({"transform":"scale(0.4)","transition":"transform 0.5s"}).animate({"right":"-="+containerWidth+"px"}, {duration:animationSpeed,complete: function(){
                $(".next, .previous").fadeIn();
                $(this).animate({"right":"+1"+containerWidth+"px"}, 1);
                $(this).animate({"right":"-="+containerWidth+"px"}, animationSpeed).css({"transform":"scale(1)","transition":"transform 0.5s"});
            }})
            
        }else{
        }
    })


    /* Main function to pull up books from the database */
    function pullBooks(){

        /* Haal JSON data op met jQuery-AJAX */
        $.getJSON("pullBooks.php?id="+id, function(data){})

        /* Als het gelukt is */
        .done(function(data){

            $("#book-container").show();

            /* Haal eerst alle waarden eruit */
            var image       = data.image;
            var title       = data.title;
            var author      = data.author;
            var edition     = data.edition;
            var description = PDFFolder+"/"+data.description+"#page="+startAtPage+"&zoom="+zoomLevel+"&view=fitH";
            
            bookImage.attr("src", image); // Book Image Source
            bookTitle.html(title);       // Book Ritle
            bookAuthor.html(author); // Book Author
            bookEdition.html("Editie: "+edition); // Book Edition

            if(description!=""){
                bookDescription.attr("src",description);
            }else{
                bookDescription.attr("src","");
            }
            
        })

        /* Als ophalen van data niet gelukt is */
        .fail(function(){
            alert("Niet gelukt om JSON op te halen");
        })

    }

    /* Main function to count books */
    function countBooks(){
        $(".count-books").html(id+"/"+maxId+" boeken in de bibliotheek");
    }

    /* Main book mouse-over & leave function */
    $("#book-container").on("mouseover", function(){
        $(this).css({
            "perspective":"1000px",
            "transform":"scale(1.1) rotateY(5deg)",
            "transition":"transform 0.45s"
        });
    }).on("mouseleave", function(){
        $(this).css({
            "transform":"scale(1)",
            "transition":"transform 0.45s"
        });
    })

    /* Main book mouse-click function */
    $("#book-container").on("click", function(){
        $("body").css({"background-image":"url('img/background2_dark.jpg')"}).fadeOut(350).fadeIn(350);
        $(this).css({
            "transform":"scale(1.6)",
            "transition":"transform 1s"
        }).fadeOut();
        $(".previous, .next").fadeOut();
        $(".count-books").css("visibility","hidden");
        $("#cross").css("visibility","visible");
        $("#book-description").fadeIn();

    })

    /* Main exit-cross mouse-click function */
    $("#cross").on("click", function(){
        $("body").css({"background-image":"url('img/background2.jpg')"}).hide().fadeIn();
        $(this).css("visibility","hidden");
        $("#book-container, .previous, .next").fadeIn();
        $(".count-books").css("visibility","visible");
        $("#book-description").css({
            "transform":"scale(1.6)",
            "transition":"transform 1s"
        }).fadeOut("fast", function(){
        $(this).css({
             "transform":"scale(1)",
                })
        });
    })

})