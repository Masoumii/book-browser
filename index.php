<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="main.js?version=1.1"></script>
    <link href="https://fonts.googleapis.com/css?family=Gentium+Book+Basic" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <title>Book-Browser</title>
</head>
<body>

    <img src="img/close.png" id="cross">


    <h1 class="count-books"></h1>

    <div id="book-description">

    <iframe scrolling="auto" style="width:99% !important;height:99%" id="bookSrc" src=""></iframe>
    

    </div>
    <div id="book-container">

        <div class="book">
            <div class="book-title"></div>
            <img class="book-image">
            <div class="book-author"></div>
            <div class="book-edition"></div>
        </div>

    </div>

<br>
        <img class="previous" src="img/previous.png">
        <img class="next" src="img/next.png">
        
</body>
</html>